package com.sallietrixiezebadamansurina_10191077.praktikum1.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.sallietrixiezebadamansurina_10191077.praktikum1.R;

public class MainActivity extends AppCompatActivity {

    TextView tv;
    Button btn, btn_kurang;
    int nilai;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv = findViewById(R.id.textView2);
        btn = findViewById(R.id.btn);
        btn_kurang = findViewById(R.id.btn_kurang);
        nilai = 0;

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tambah();
            }
        });

        btn_kurang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kurang();
            }
        });
    }

    private void tambah() {
        nilai++;
        tv.setText(String.valueOf(nilai));
    }
    private void kurang() {
        nilai--;
        tv.setText(String.valueOf(nilai));
    }
}